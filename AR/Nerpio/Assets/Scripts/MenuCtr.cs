﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuCtr : MonoBehaviour {

    [SerializeField]
    Canvas _activeCanvas;

    [SerializeField]
    Canvas menuCanvas;

    [SerializeField]
    string startLanguage;

    [SerializeField]
    Image btnCt, btnEs, btnEn;


    [SerializeField]
    bool showAnimation = false;
    [SerializeField]
    float changeMenuSpeed = 2f;

    static bool inited = false;

    Camera _camera;
    Dictionary<string, Image> _languageBtns;

    Color _btnLanNoSelColor = new Color(1, 1, 1, .25f);

    private void Awake() {

        _camera = Camera.main;

        if(_activeCanvas) {
            _activeCanvas.gameObject.SetActive(true);
        }


        _languageBtns = new Dictionary<string, Image>(3);
        _languageBtns.Add("es_CT", btnCt);
        _languageBtns.Add("es_ES", btnEs);
        _languageBtns.Add("en_GB", btnEn);


        if(!inited) {
            if(startLanguage.Length > 0)
                changeLanguage(startLanguage);
                //LocalizationCtr.getInstance().setLanguage(startLanguage);

            inited = true;
        } else {
            updateLanguageBtns();
        }
    }

    public void changeScene(string scene) {
        SceneManager.LoadScene(scene);
    }

    public void changeCanvas(Canvas canvas) {


        canvas.gameObject.SetActive(true);

        if(showAnimation) {
            StartCoroutine(canvasTrans(canvas));
        } else {
            if(_activeCanvas) {
                _activeCanvas.gameObject.SetActive(false);
            }
            
            _activeCanvas = canvas;
        }
    }

    IEnumerator canvasTrans(Canvas canvas) {

        Transform child = canvas.transform.GetChild(0);

        Vector2 startPos;
        Vector2 endPos;

        if(canvas == menuCanvas) {
            child = _activeCanvas.transform.GetChild(0);

            startPos = new Vector2(_camera.pixelWidth / 2f, child.position.y);
            endPos = new Vector2(_camera.pixelWidth * 1.5f, child.position.y);

            canvas.sortingOrder = 0;
            _activeCanvas.sortingOrder = 1;
        } else {
            startPos = new Vector2(_camera.pixelWidth * 1.5f, child.position.y);
            endPos = new Vector2(_camera.pixelWidth / 2f, child.position.y);

            canvas.sortingOrder = 1;
        }

        child.position = startPos;

        float e = 0;

        while(e <= 1.5f) {

            child.position = Vector2.Lerp(startPos, endPos, e);

            e += Time.deltaTime * changeMenuSpeed;

            yield return new WaitForEndOfFrame();
        }
        child.position = endPos;

        if(_activeCanvas) {
            _activeCanvas.gameObject.SetActive(false);
            _activeCanvas.sortingOrder = 0;
        }

        _activeCanvas = canvas;
        canvas.sortingOrder = 0;

    }


    public void changeLanguage(string lang) {
        LocalizationCtr.getInstance().setLanguage(lang);

        updateLanguageBtns();
    }

    void updateLanguageBtns() {

        foreach(var item in _languageBtns) {
            item.Value.color = _btnLanNoSelColor;
        }

        _languageBtns[LocalizationCtr.getInstance().ActiveLanguage].color = Color.white;
    }

}
