﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestCanvas : MonoBehaviour{

    [SerializeField]
    Text groupText;

    [SerializeField]
    Transform activeList;

    [SerializeField]
    Text activeTextPre;

    [SerializeField]
    ARTargetElement[] elements;

    Dictionary<string, Text> elementsTexts;

    // Start is called before the first frame update
    void Start() {
        elementsTexts = new Dictionary<string, Text>();
        Text created;

        foreach(var item in elements) {
            created = Instantiate(activeTextPre);
            created.text = $"{item.name}\t[{item.TargetGroup}]";
            created.transform.SetParent(activeList);

            elementsTexts.Add(item.name, created);
        }
    }

    // Update is called once per frame
    void Update(){
        groupText.text = $"Group: {TargetGroupsCtr.ActiveGroup}";

        foreach(var item in elements) {
            if(item.isVisible) {
                elementsTexts[item.name].color = Color.green;
            } else {
                elementsTexts[item.name].color = Color.red;
            }
        }
    }
}
