﻿using Shared.CameraUtils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcosCnv : MonoBehaviour{

    [SerializeField]
    TouchCamera _camera;

    private void Awake() {

        _camera.enabled = false;
    }

    private void OnEnable() {
        CalcosCtr.INSTANCE.showCalco(true);

        _camera.enabled = true;
    }

    private void OnDisable() {
        CalcosCtr.INSTANCE.showCalco(false);

        _camera.enabled = false;
    }
}
