﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcosMenu : MonoBehaviour {

    [SerializeField]
    GameObject[] menus;

    public void changeMenu(GameObject menu) {

        foreach(var item in menus) {
            item.SetActive(item == menu);
        }

    }
}

