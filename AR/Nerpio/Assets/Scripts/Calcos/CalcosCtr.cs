﻿using Shared.CameraUtils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalcosCtr : MonoBehaviour{

    public static CalcosCtr INSTANCE;

    [SerializeField]
    GameObject calco = null;

    [SerializeField]
    Camera _camera = null;

    CameraLimits2D _cameraLimits = null;

    [SerializeField]
    float cameraZoom = 2;
    [SerializeField]
    float zoomSpeed = 20;

    [SerializeField]
    float maxZoom = 10;

    [SerializeField]
    Button btnZoomPlus = null, btnZoomMinus = null;

    [SerializeField]
    SpriteRenderer _calcoRenderer = null;

    float _nextSize = -1;
    Vector2 _selectedSize;

    private void Awake() {
        INSTANCE = this;

        showCalco(false);

        _cameraLimits = _camera.GetComponent<CameraLimits2D>();

        //_camera.orthographicSize = maxZoom;
    }

    public void showCalco(bool show) {

        calco.SetActive(show);

        if(_cameraLimits) {
            //_cameraLimits.area.height = 61.6f + (maxZoom * 6.3f) / 25.2f;
            _camera.orthographicSize = _cameraLimits.MaxZoom;
            _camera.transform.position = _cameraLimits.CameraCenterPos;
        }
    }

    private void Update() {
        _camera.orthographicSize += _nextSize * Time.deltaTime * zoomSpeed;
        _nextSize -= _nextSize * Time.deltaTime * zoomSpeed;

        if(_camera.orthographicSize < _cameraLimits.MinZoom) _camera.orthographicSize += (_cameraLimits.MinZoom - _camera.orthographicSize) * Time.deltaTime * zoomSpeed;

        limitZoom();

        updateLimits(_selectedSize);

    }

    public void selectCalco(Sprite calcoTexture) {
        _nextSize = 0;

        _calcoRenderer.sprite = calcoTexture;
        updateLimits(calcoTexture.rect.size);

        _camera.orthographicSize = _cameraLimits.MaxZoom;
    }

    public void zoom(int dir) {
        _nextSize = cameraZoom * -dir;

    }

    void updateLimits(Vector2 size) {
        Rect r = new Rect();
        _selectedSize = size;

        r.size = _selectedSize * 80 / 2048;
        r.center = _calcoRenderer.transform.position;

        r.size += new Vector2(0, _camera.orthographicSize * .25f);

        _cameraLimits.setAreaLimits(r);
    }

    void limitZoom() {

        _camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize, _cameraLimits.MinZoom, maxZoom);

        btnZoomPlus.interactable = _camera.orthographicSize > _cameraLimits.MinZoom + .1f;
        btnZoomMinus.interactable = _camera.orthographicSize < maxZoom - .1f;
    }


}
