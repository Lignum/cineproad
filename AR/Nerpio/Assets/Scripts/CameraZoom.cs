﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class CameraZoom : MonoBehaviour{

    [SerializeField]
    Camera refCamera;

    [SerializeField]
    float zoom;

    RenderTexture rt;
    RawImage _image;

    bool _active = true;


    // Start is called before the first frame update
    void Awake(){
        rt = new RenderTexture(refCamera.pixelWidth, refCamera.scaledPixelHeight, 24);
        _image = GetComponent<RawImage>();
        _image.texture = rt;

        setZoom(zoom);
    }

    void Update(){
        if(!_active) return;

        if(refCamera.targetTexture == null) {
            refCamera.targetTexture = rt;
            refCamera.Render();

            refCamera.targetTexture = null;
        }
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
        _image.transform.localScale = new Vector3(zoom, zoom, 1);
    }

    public void setActive(bool active) {
        _active = active;
    }
}
