﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.CameraUtils {

    public class CameraLimits2D : MonoBehaviour {


        [SerializeField]
        Rect area;

        [SerializeField]
        float minZoom = 10;
        public float MinZoom {
            get { return minZoom; }
        }

        public float MaxZoom {
            get {
                if(useBreakDistanceForZoom) {
                    return _maxBreakDistanceRect.height / 2;
                } else {
                    return area.height / 2;
                }
            }
        }

        public Vector3 CameraCenterPos {
            get { return new Vector3(area.center.x, area.center.y, -10); }
        }

        [SerializeField]
        float maxBreakDistance = 10;
        [SerializeField]
        bool useBreakDistanceForZoom = true;

        [SerializeField]
        float lerpSpeed = -1;

        public bool FixPosition = true;

        Camera _camera;

        Rect _cameraRect = new Rect();
        Rect _maxBreakDistanceRect = new Rect();

        Vector2 _corDirecton;
        float _corZoomWidth;
        float _corZoomHeight;

        bool _blockTranslate;


        private void Awake() {

            _camera = GetComponent<Camera>();
            if(!_camera) {
                _camera = GetComponentInChildren<Camera>();
                if(_camera == null)
                    Debug.LogError("CameraLimits2D Needs a Camera or a Camera child", gameObject);
            }

            calculateMaxBreakDistance();
        }

        private void OnValidate() {
            calculateMaxBreakDistance();
        }


        // Update is called once per frame
        private void LateUpdate() {


            _cameraRect.height = _camera.orthographicSize * 2;
            _cameraRect.width = _cameraRect.height * Screen.width / Screen.height;

            _cameraRect.position = (Vector2)transform.position - (_cameraRect.size / 2);


            limitPosition();
            limitZoom();

        }

        public void setAreaLimits(Rect limits) {
            area = limits;
            calculateMaxBreakDistance();
        }

        void limitPosition() {


            if(lerpSpeed > 0) {

                _corDirecton = Vector2.zero;
                _blockTranslate = false;

                if(_cameraRect.yMin < _maxBreakDistanceRect.yMin) _corDirecton += Vector2.up * (_maxBreakDistanceRect.yMin - _cameraRect.yMin);
                if(_cameraRect.xMin < _maxBreakDistanceRect.xMin) _corDirecton += Vector2.right * (_maxBreakDistanceRect.xMin - _cameraRect.xMin);
                if(_cameraRect.yMax > _maxBreakDistanceRect.yMax) _corDirecton += Vector2.down * (_cameraRect.yMax - _maxBreakDistanceRect.yMax);
                if(_cameraRect.xMax > _maxBreakDistanceRect.xMax) _corDirecton += Vector2.left * (_cameraRect.xMax - _maxBreakDistanceRect.xMax);

                transform.Translate(_corDirecton);
            } else {
                _blockTranslate = true;
            }

            if(FixPosition) {
                _corDirecton = Vector2.zero;
                if(_cameraRect.yMin < area.yMin) _corDirecton += Vector2.up * (area.yMin - _cameraRect.yMin);
                if(_cameraRect.xMin < area.xMin) _corDirecton += Vector2.right * (area.xMin - _cameraRect.xMin);
                if(_cameraRect.yMax > area.yMax) _corDirecton += Vector2.down * (_cameraRect.yMax - area.yMax);
                if(_cameraRect.xMax > area.xMax) _corDirecton += Vector2.left * (_cameraRect.xMax - area.xMax);

                if(_cameraRect.xMin <= area.xMin && _cameraRect.xMax >= area.xMax) {
                    _corDirecton.x = 0;
                    transform.position = new Vector3(area.center.x, transform.position.y, transform.position.z);
                }
                if(_cameraRect.yMin <= area.yMin && _cameraRect.yMax >= area.yMax) {
                    _corDirecton.y = 0;
                    transform.position = new Vector3(transform.position.x, area.center.y, transform.position.z);
                }

                if(_blockTranslate) {
                    transform.Translate(_corDirecton);
                } else {
                    transform.Translate(_corDirecton * lerpSpeed * Time.deltaTime);
                }
            }
        }

        void limitZoom() {

            _corZoomWidth = 0;
            _corZoomHeight = 0;

            if(lerpSpeed > 0) {
                _blockTranslate = false;
                if(_cameraRect.width > _maxBreakDistanceRect.width) _corZoomWidth = _maxBreakDistanceRect.width - _cameraRect.width;
                if(_cameraRect.height > _maxBreakDistanceRect.height) _corZoomHeight = _maxBreakDistanceRect.height - _cameraRect.height;

                _camera.orthographicSize += Mathf.Min(_corZoomWidth / (Screen.width / Screen.height) / 2, _corZoomHeight / 2);
            } else {
                _blockTranslate = true;
            }

            if(FixPosition) {

                Rect reference;
                if(useBreakDistanceForZoom) {
                    reference = _maxBreakDistanceRect;
                } else {
                    reference = area;
                }

                _corDirecton = Vector2.zero;
                if(_cameraRect.width > reference.width) _corZoomWidth = reference.width - _cameraRect.width;
                if(_cameraRect.height > reference.height) _corZoomHeight = reference.height - _cameraRect.height;

                if(_blockTranslate) {
                    _camera.orthographicSize += Mathf.Min(_corZoomWidth / (Screen.width / Screen.height) / 2, _corZoomHeight / 2);
                } else {
                    _camera.orthographicSize += Mathf.Min(_corZoomWidth / (Screen.width / Screen.height) / 2, _corZoomHeight / 2) * lerpSpeed*.5f * Time.deltaTime;
                }
            }
        }
        
        void calculateMaxBreakDistance() {
            _maxBreakDistanceRect = new Rect(area);

            _maxBreakDistanceRect.position -= Vector2.one * maxBreakDistance / 2;
            _maxBreakDistanceRect.size += Vector2.one * maxBreakDistance;
        }

        private void OnDrawGizmosSelected() {

            Gizmos.color = Color.blue;
            CustomGizmos.drawRectagle(area);

            Gizmos.color = Color.red;
            CustomGizmos.drawRectagle(_maxBreakDistanceRect);

            Gizmos.color = Color.green;
            CustomGizmos.drawRectagle(_cameraRect);
        }
    }
}