﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleUI : MonoBehaviour{

    [SerializeField]
    GameObject target;

    Image _image;


    private void Awake() {
        _image = GetComponent<Image>();
        updateButtonColor();
    }

    public void toggleActive() {

        target.SetActive(!target.activeSelf);
        updateButtonColor();
    }

    void updateButtonColor() {

        if(target.activeSelf) {
            _image.color = GlobalData.SelectedColor;
        } else {
            _image.color = GlobalData.UnSelectedColor;
        }
    }

}
