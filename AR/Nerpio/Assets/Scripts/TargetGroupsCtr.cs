﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TargetGroupsCtr{

    static Dictionary<int, List<ARTargetElement>> _groups = new Dictionary<int, List<ARTargetElement>>();

    public static int ActiveGroup { get; private set; }

    public static void addElement(ARTargetElement element) {
        if(!_groups.ContainsKey(element.TargetGroup)) {
            _groups.Add(element.TargetGroup, new List<ARTargetElement>());
        }
        
        _groups[element.TargetGroup].Add(element);

    }

    static int getVisibleGroup() {
        for(int i = 0; i < _groups.Count; i++) {
            if(isGroupVisible(i)) return i;
        }
        return -1;
    }

    public static void updateActiveGroup(int group) {
        if(group == -1) return;

        if(group != ActiveGroup) {
            ActiveGroup = getVisibleGroup();
            for(int i = 0; i < _groups.Count; i++) {
                if(!_groups.ContainsKey(i)) continue;
                foreach(var item in _groups[i]) {
                    item.updateElement();
                }
            }
        }
    }


    public static bool checkGroup(int group) {
        if(group == -1) return true;

        for(int i = 0; i < group; i++) {
            if(isGroupVisible(i)) return false;
        }
        return true;
    }

    public static bool isGroupVisible(int group) {

        if(group == -1) return true;
        if(!_groups.ContainsKey(group)) return false;

        foreach(var item in _groups[group]) {

            if(item.isVisible) return true;
        }
        return false;
    }

}
