﻿using Shared.CameraUtils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalcosCtr : MonoBehaviour{

    public static CalcosCtr INSTANCE;

    [SerializeField]
    GameObject calco;

    [SerializeField]
    Camera _camera;

    CameraLimits2D _cameraLimits;

    [SerializeField]
    float cameraZoom = 2;
    [SerializeField]
    float zoomSpeed = 20;

    [SerializeField]
    float maxZoom = 10;

    [SerializeField]
    Button btnZoomPlus, btnZoomMinus;

    float _nextSize = -1;

    private void Awake() {
        INSTANCE = this;

        showCalco(false);

        _cameraLimits = _camera.GetComponent<CameraLimits2D>();

        //_camera.orthographicSize = maxZoom;
    }

    public void showCalco(bool show) {

        calco.SetActive(show);

        if(_cameraLimits) {
            _cameraLimits.area.height = 61.6f + (maxZoom * 6.3f) / 25.2f;
            _camera.orthographicSize = _cameraLimits.MaxZoom;
            _camera.transform.position = _cameraLimits.CameraCenterPos;
        }
    }

    private void Update() {
        _camera.orthographicSize += _nextSize * Time.deltaTime * zoomSpeed;
        _nextSize -= _nextSize * Time.deltaTime * zoomSpeed;

        if(_camera.orthographicSize < _cameraLimits.MinZoom) _camera.orthographicSize += (_cameraLimits.MinZoom - _camera.orthographicSize) * Time.deltaTime * zoomSpeed;

        _cameraLimits.area.height = 61.6f + (_camera.orthographicSize * 6.3f) / 25.2f;

        limitZoom();
    }

    public void zoom(int dir) {
        _nextSize = cameraZoom * -dir;
    }

    void limitZoom() {

        _camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize, _cameraLimits.MinZoom, maxZoom);

        btnZoomPlus.interactable = _camera.orthographicSize > _cameraLimits.MinZoom + .1f;
        btnZoomMinus.interactable = _camera.orthographicSize < maxZoom - .1f;

        /*  if(_camera.orthographicSize < minZoom) {
             if(lerpSeed > 0) {
                  _camera.orthographicSize += (minZoom - _camera.orthographicSize) * Time.deltaTime * lerpSeed;
              } else {
                  _camera.orthographicSize = minZoom;
              //}
          }*/
    }


}
