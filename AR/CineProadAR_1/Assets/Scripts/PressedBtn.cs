﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressedBtn : MonoBehaviour{

    MaskableGraphic _graphic;

    private void Awake() {
        _graphic = GetComponent<MaskableGraphic>();
    }

    public void onPress() {
        _graphic.color = GlobalData.PressedColor;
    }

}
