﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ARTargetElement : MonoBehaviour, ITrackableEventHandler {

    private TrackableBehaviour mTrackableBehaviour;

    [SerializeField]
    int targetGroup;
    public int TargetGroup {
        get {
            return targetGroup;
        }
    }

    Renderer[] variations;
    MeshRenderer _renderer;

    bool _visibleGroup;

    public bool isVisible {
        get {
            return variations[0].enabled || variations[1].enabled;
        }
    }

    private void Awake() {
        _renderer = GetComponent<MeshRenderer>();
        variations = new Renderer[2];
        variations[0] = transform.GetChild(0).GetComponent<Renderer>();
        variations[1] = transform.GetChild(1).GetComponent<Renderer>();
    }

    // Start is called before the first frame update
    void Start(){

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if(mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }


        ARCtr.INSTANCE.addElement(this);

    }
    
    public void updateElement() {
        foreach(var item in variations) {
            item.gameObject.SetActive(false);
        }

        variations[ARCtr.SelectedVar].gameObject.SetActive(TargetGroupsCtr.ActiveGroup >= TargetGroup);
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus) {
        ARCtr.INSTANCE.onElementTrackStateChange(this, newStatus);

        TargetGroupsCtr.updateActiveGroup(TargetGroup);

        updateElement();
    }
}
