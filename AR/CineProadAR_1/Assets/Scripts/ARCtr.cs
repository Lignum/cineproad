﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ARCtr : MonoBehaviour{

    public static ARCtr INSTANCE;
    public static int SelectedVar { get; private set; } = 0;
    static List<ARTargetElement> _elements;

    [SerializeField]
    UnityEngine.UI.Image[] buttons;

    private void Awake() {
        INSTANCE = this;
        _elements = new List<ARTargetElement>();
    }

    private void Start() {
        setVariation(0);
    }

    public void addElement(ARTargetElement element) {
        _elements.Add(element);
        element.updateElement();

        TargetGroupsCtr.addElement(element);
    }

    public void setVariation(int var) {
        SelectedVar = var;

        for(int i = 0; i < buttons.Length; i++) {
            buttons[i].color = GlobalData.UnSelectedColor;
        }
        buttons[var].color = GlobalData.SelectedColor;

        foreach(var item in _elements) {
            item.updateElement();
        }
    }

    /*private void Update() {
        cubeTest0.material.color = Color.blue;
        cubeTest1.material.color = Color.blue;
        if(TargetGroupsCtr.isGroupVisible(0)) {
            cubeTest0.material.color = Color.green;
            if(TargetGroupsCtr.checkGroup(0)) {
                cubeGroup.material.color = Color.green;
            }
        }
        if(TargetGroupsCtr.isGroupVisible(1)) {
            cubeTest1.material.color = Color.red;
            if(TargetGroupsCtr.checkGroup(1)) {
                cubeGroup.material.color = Color.red;
            }
        }
    }*/

    public void onElementTrackStateChange(ARTargetElement element, TrackableBehaviour.Status newStatus) {
            
    }
}
