﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizatedUIText : MonoBehaviour{

    Text _text;
    string _textKey;

    private void Awake() {
        LocalizationCtr.getInstance().addLocalizatedText(this);

        _text = GetComponent<Text>();
        _textKey = _text.text;

        updateText();
    }

    private void OnDestroy() {
        LocalizationCtr.getInstance().removeLocalizatedText(this);
    }

    public void updateText() {
        _text.text = LocalizationCtr.getInstance().getText(_textKey);
    }


}
