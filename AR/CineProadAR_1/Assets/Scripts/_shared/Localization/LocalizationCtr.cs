﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationCtr{

    static LocalizationCtr INSTANCE;

    public string ActiveLanguage { get; private set; }

    static Dictionary<string, TextAsset> _languages = new Dictionary<string, TextAsset>();
    static Dictionary<string, string> _activeDictionary = new Dictionary<string, string>();

    List<LocalizatedUIText> _localizatedTexts = new List<LocalizatedUIText>();

    private LocalizationCtr() {

        TextAsset[] texts = Resources.LoadAll<TextAsset>("Localization");

        string firstLan = null;

        foreach(var item in texts) {
            _languages.Add(item.name, item);

            if(firstLan == null) firstLan = item.name;

        }

        setLanguage(firstLan);

    }

    public void addLocalizatedText(LocalizatedUIText locText) {
        _localizatedTexts.Add(locText);
    }
    public void removeLocalizatedText(LocalizatedUIText locText) {
        _localizatedTexts.Remove(locText);
    }

    public static LocalizationCtr getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new LocalizationCtr();
        }

        return INSTANCE;
    }

    public void setLanguage(string language) {

        if(_languages.ContainsKey(language)) {
            ActiveLanguage = language;

            setActiveDictionary(ActiveLanguage);

            updateLocalizatedTexts();
        } else {
            Debug.LogError($"Unknow language: {language}");
        }
    }

    public string getText(string key) {
        if(_activeDictionary.ContainsKey(key)) {
            return _activeDictionary[key];
        } else {
            return $"#'{key}'";
        }
    }

    void setActiveDictionary(string languageKey) {

        string[] lines = _languages[languageKey].text.Split('\n');
        
        string key;
        string value;

        _activeDictionary.Clear();

        foreach(var line in lines) {

            key = line.Split('=')[0];

            if(key.Length == 0) continue;

            value = line.Replace(key+"=", "");

            _activeDictionary.Add(key.Replace(" ", ""), value);
        }

    }

    void updateLocalizatedTexts() {
        foreach(var item in _localizatedTexts) {
            item.updateText();
        }
    }

}
