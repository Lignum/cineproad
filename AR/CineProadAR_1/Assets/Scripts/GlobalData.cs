﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalData{

    public static Color SelectedColor { get; } = Color.white;
    public static Color UnSelectedColor { get; } = new Color(1, 1, 1, .25f);

    public static Color PressedColor { get; } = new Color(0.69f, 0.64f, 0.55f);

}
