﻿using UnityEngine;
using System.Collections;

public class VuforiaFocus : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }
	
}
