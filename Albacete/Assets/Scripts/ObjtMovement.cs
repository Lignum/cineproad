﻿using UnityEngine;
using System.Collections;

public class ObjtMovement : MonoBehaviour {

    public float height = .1f;
    public float rotationSpeed = 10;

    Vector3 initPos;
    bool up = true;


	// Use this for initialization
	void Start () {
        initPos = transform.localPosition;
        StartCoroutine(verticalMovement());
        StartCoroutine(rotationMovement());
    }
	
    IEnumerator verticalMovement() {

        float time = Random.Range(0, Mathf.PI*2);
        Vector3 targetPos = initPos + Vector3.up * height;

        while (true) {

            transform.localPosition = Vector3.Lerp(initPos, targetPos, (Mathf.Sin(time) + 1) / 2);

            time += Time.deltaTime;
            if (time > Mathf.PI*2) {
                time = 0;
            }

            yield return new WaitForEndOfFrame();

        }
    }

    IEnumerator rotationMovement() {

        while (true) {

            transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);

            yield return new WaitForEndOfFrame();
        }

    }
}
