﻿using UnityEngine;
using System.Collections;

public class ARTabShowText : MonoBehaviour {

    GameObject child;

    void Start() {
        child = transform.GetChild(0).gameObject;
    }
    
    public void activate() {
        child.SetActive(!child.activeSelf);
    }

}
