﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ChangeScene : MonoBehaviour {
    
    public void change(int scene) {
        SceneManager.LoadScene(scene);
    }
}
