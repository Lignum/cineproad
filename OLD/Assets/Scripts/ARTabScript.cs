﻿using UnityEngine;
using System.Collections;

public class ARTabScript : MonoBehaviour {

    Ray ray;
    RaycastHit hit;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0)) {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)){
                hit.collider.GetComponent<ARTabShowText>().activate();

            }

            //Debug.DrawRay(pos.origin, pos.direction);
            //Debug.Log(pos);
        }
	
	}
}
